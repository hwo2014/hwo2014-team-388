package dmd.domain;

public class Message <T>{
	
	public String msgType;
    public T data;
    public String gameId;
    public int gameTick;

}
