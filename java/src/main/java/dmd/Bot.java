package dmd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import dmd.domain.CarId;
import dmd.domain.CarPosition;
import dmd.domain.Message;
import dmd.domain.Race;
import dmd.domain.TrackPiece;
import dmd.domain.TurboAvaiable;
import dmd.graph.ConstantThrottleController;
import dmd.graph.LinearThrottleController;
import dmd.graph.SwitchController;
import dmd.graph.ThrottleController;
import dmd.handler.CarPositionsHandler;
import dmd.handler.CrashHandler;
import dmd.handler.DNFHandler;
import dmd.handler.FinishHandler;
import dmd.handler.GameEndHandler;
import dmd.handler.GameInitHandler;
import dmd.handler.GameStartHandler;
import dmd.handler.JoinHandler;
import dmd.handler.JoinRaceHandler;
import dmd.handler.LapFinishedHandler;
import dmd.handler.MessageHandler;
import dmd.handler.SpawnHandler;
import dmd.handler.TournamentEndHandler;
import dmd.handler.TurboAvaiableHandler;
import dmd.handler.TurboEndHandler;
import dmd.handler.TurboStartHandler;
import dmd.handler.YourCarHandler;

public class Bot {

	private BufferedReader reader;
	private PrintWriter writer;
	
	public String botName;
	public String botKey;
	
	public int currentTick = -1;
	public int lap = 0;
	
	public double speed;
    public double acceleration;
	public double angle;
	public double previousAngle;
	public double throttle;
	public CarPosition lastPosition;
	public TrackPiece currentTrackPiece;
	public boolean hasTurbo;
	public TurboAvaiable turbo;
	public boolean isTurboOn;

	/**
	 * controla quais tick tiveram resposta
	 */
	public boolean[] msgOnTick = new boolean[100000];
	
	public final Gson gson = new Gson();
	
	public List<Integer> minPath;
	
	public CarId id;
	public Race race;
	
	public Logger logger;
	
	protected final Map<String, MessageHandler> handlers = new HashMap<String, MessageHandler>();
	public Bot(BufferedReader reader, PrintWriter writer, ThrottleController throttleController, SwitchController switchController) {
		
		handlers.put("join", new JoinHandler());
		handlers.put("joinRace", new JoinRaceHandler());
		handlers.put("yourCar", new YourCarHandler());		
		handlers.put("carPositions", new CarPositionsHandler(throttleController, switchController));
		handlers.put("gameStart", new GameStartHandler());
		handlers.put("gameInit", new GameInitHandler());
		handlers.put("gameEnd", new GameEndHandler());
		handlers.put("tournamentEnd", new TournamentEndHandler());
		handlers.put("crash", new CrashHandler());
		handlers.put("spawn", new SpawnHandler());
		handlers.put("lapFinished", new LapFinishedHandler());
		handlers.put("dnf", new DNFHandler());
		handlers.put("finish", new FinishHandler());	
		handlers.put("turboAvailable", new TurboAvaiableHandler());
		handlers.put("turboStart", new TurboStartHandler());
		handlers.put("turboEnd", new TurboEndHandler());
		
		this.reader = reader;
		this.writer = writer;
	}

	public void go(String track) throws IOException {
		if (track != null) {
            join(track);
        } else {
            join();
        }

		String line = null;
		while((line = reader.readLine()) != null){
			
			Message<?> m = gson.fromJson(line,Message.class);
			
			currentTick = m.gameTick;				
			
			MessageHandler handler = handlers.get(m.msgType);
			if(handler != null) {
				handler.handle(line, this);
			} else {
				System.out.println("Mensagem não esperada!");
				System.out.println(line);
			}
			
			if(currentTick >= 0 && !msgOnTick[currentTick]){
				ping(currentTick);
			}
			
			if(logger != null) {
				logger.log(this);
			}
		}
	}
	
	public void join() {
		send("{\"msgType\": \"join\", \"data\": {\"name\": \"" + botName + "\", \"key\": \"" + botKey + "\"}}");
	}
	
	public void join(String trackname){
		send("{\"msgType\": \"joinRace\", \"data\": {\"botId\": {\"name\": \"" + botName+ "\",\"key\": \"" + botKey + "\"},\"trackName\": \"" + trackname + "\",\"carCount\": 1}}");
	}
	
	public void throttle(double throttle, int gameTick){
		this.throttle = throttle;
		send("{\"msgType\": \"throttle\", \"data\": " + throttle + ", \"gameTick\": " + gameTick + "}");
	}
	
	
	public void switchLane(boolean toRight, int gameTick){
		if(toRight){
			send("{\"msgType\": \"switchLane\", \"data\": \"Right\", \"gameTick\": " + gameTick + "}");
		} else {
			send("{\"msgType\": \"switchLane\", \"data\": \"Left\", \"gameTick\": " + gameTick + "}");
		}
	}
	
	public void ping(int gameTick){
		send("{\"msgType\": \"ping\", \"gameTick\": " + gameTick + "}");
	}
	
	public void turbo(int gameTick){
		hasTurbo = false;
		send("{\"msgType\": \"turbo\", \"data\": \"Auuuuuuuuuuuuuuuuuuuuuuuuuuuuuu!!!\", \"gameTick\": " + gameTick + "}");
	}
	
	protected void send(String json){
		writer.println(json);
        writer.flush();

        if(currentTick >=0){
        	msgOnTick[currentTick] = true;
        }
	}
}
