package dmd.domain;


public class PiecePosition {
	
    public int pieceIndex;
    public double inPieceDistance;
    public LanePosition lane;
    public int lap;
    
	@Override
	public String toString() {
		return "PiecePosition [pieceIndex=" + pieceIndex + ", inPieceDistance="
				+ inPieceDistance + ", lane=" + lane + ", lap=" + lap + "]";
	}
    
    

}
