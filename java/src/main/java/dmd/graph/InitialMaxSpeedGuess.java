package dmd.graph;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dmd.domain.Lane;
import dmd.domain.Track;
import dmd.domain.TrackPiece;

public class InitialMaxSpeedGuess {
	
	public void guess(Track track) {
		
		TrackPiece inicio = track.pieces.get(0);
		TrackPiece current = inicio.nextTrackPiece;
		
		//preenche primeiro as curvas
		while(current.indice != inicio.indice){
			
			//saiu de uma curva
			if(!current.ehContinuacaoCurva() && current.previousTrackPiece.ehCurva()) {
				
				List<TrackPiece> curva = current.previousTrackPiece.obterCurvaCompleta();
				
				for(TrackPiece curvaPiece : curva){
					//nao tirar a instanciação desse maldito array de dentro do for
					//caso contrario o array sera compartilhado com outros pieces e atualização será um caos!
					double[] maxSpeed = guessMaxSpeed(curva, track.lanes);
					curvaPiece.maxSpeed = maxSpeed;
				}
			}
			
			current = current.nextTrackPiece;
		}
		
		//preenche as retas em função das curvas
		TrackPiece primeiraCurva = inicio.nextTrackPiece;
		while(!primeiraCurva.ehCurva()){
			primeiraCurva = primeiraCurva.nextTrackPiece;
		}
		
		current = primeiraCurva.previousTrackPiece;
		while(current.indice != primeiraCurva.indice){
			
			if(!current.ehCurva()){

				current.maxSpeed = new double[track.lanes.size()];
				for(Lane lane : track.lanes){
					current.maxSpeed[lane.index] = current.nextTrackPiece.maxSpeed[lane.index]*1.20;
				}
			}
			
			current = current.previousTrackPiece;
		}
	}
	
	protected double[] guessMaxSpeed(List<TrackPiece> curva, List<Lane> lanes){
		
		double[] max = new double[lanes.size()];
		double angulo = TrackPiece.calculaAnguloTotalCurva(curva);
		
		for(Lane lane : lanes) {
			
			double raio;
			if(curva.get(0).angle > 0){
				raio = curva.get(0).radius - lane.distanceFromCenter;
			}else {
				raio = curva.get(0).radius + lane.distanceFromCenter;
			}
			
			max[lane.index] = maxSpeed(angulo, raio);
		}
		
		return max;
	}
	
	/**
	keimola	22.5	190	8.72
	keimola	22.5	210	8.06
	keimola	90	90	7.03
	keimola	90	110	7.47
	keimola	180	90	6.79
	keimola	180	110	7.38
	*/
	static Map<String, Double> guess = new HashMap<String, Double>();
	static {
		guess.put("22.5_190.0", 8.72*60);
		guess.put("22.5_210.0", 8.06*60);
		guess.put("90.0_90.0", 7.03*60);
		guess.put("90.0_110.0", 7.47*60);
		guess.put("180.0_90.0", 6.79*60);
		guess.put("180.0_110.0", 7.38*60);
	}
	
	protected double maxSpeed(double angulo, double raio) {
	
//		return 300 + (raio/angulo)*10;
		
		double vmax = 580.0;
		double vmin = 278.0;
		double rmax = 220.0;
		double rmin = 40;
		
		return ((vmax-vmin)/(rmax-rmin))*(raio - rmin) + vmin;
		
//		return 100/150 * (raio - 50) + 200;		
		
		/*
		DecimalFormat formatter = new DecimalFormat(".0");
		
		String anguloTotalFmt = formatter.format(angulo).replace(',', '.'); 
		String raioFmt =  formatter.format(raio).replace(',', '.');
		String key = anguloTotalFmt + "_" + raioFmt;
		
		Double max = guess.get(key);
		if(max == null) {
			System.out.println("Deu ruim! " + key);
		}
		
		return max*0.85;
		*/
	}
}
