package dmd.graph;

import dmd.Bot;
import dmd.domain.CarPosition;
import dmd.domain.TrackPiece;

public interface SwitchController {

	public boolean shouldSwitch(Bot bot, CarPosition myPosition, TrackPiece currentPiece);
	
}
