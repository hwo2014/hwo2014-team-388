package dmd.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TrackPiece {

    public int indice;

	public double length;
	@SerializedName("switch")
	public boolean canSwitch;
	public double radius;
	public double angle;
	
	//maxSpeed por lane
	public double maxSpeed[];
	
	public TrackPiece nextTrackPiece;
	public TrackPiece previousTrackPiece;
	
	public double bestSpeed = -1;
	public double bestAngle = -1;
	
	public boolean hadCrash;

    public double peso() {
        return peso(0);
    }
       
    public boolean ehContinuacaoCurva(){
    	
    	boolean raioIgual = this.radius == previousTrackPiece.radius;
    	
    	//sinais diferentes um produto negativo, sinais iguais um produto positivo 
    	boolean mesmoSentido = (this.angle * previousTrackPiece.angle) > 0;
    	
    	return raioIgual && mesmoSentido && previousTrackPiece.ehCurva();    	
    }
    
    public List<TrackPiece> obterCurvaCompleta(){
    	
    	List<TrackPiece> curvaPieces = new ArrayList<TrackPiece>();
		
    	TrackPiece curva = this;
		while(curva.ehContinuacaoCurva()){
			curvaPieces.add(curva);
			curva = curva.previousTrackPiece;
		}
		curvaPieces.add(curva);
				
		return curvaPieces;
    }
    
    public static double calculaAnguloTotalCurva(List<TrackPiece> curva) {
		
		double angulo = 0.0;
		
		for(TrackPiece piece : curva){
			angulo += Math.abs(piece.angle);			
		}
				
		return angulo;		
	}
    
    public static double calculaBestAngleTotalCurva(List<TrackPiece> curva) {
    	
    	double slipAngle = 0.0;
		
		for(TrackPiece piece : curva){
			slipAngle = Math.max(slipAngle, piece.bestAngle)			;
		}
				
		return slipAngle;
    }
    
    public static boolean calculaHadCrashTotalCurva(List<TrackPiece> curva) {
    	
		for(TrackPiece piece : curva) {
			if(piece.hadCrash){
				return true;
			}
		}
		return false;
    }

    public double peso(double diferencaRaio) {
        if (length != 0) {
            return length;
        }

        if (angle != 0 && radius != 0) {
            if (angle > 0) {
                return (2 * Math.PI * Math.abs(angle) / 360.00) * (radius - diferencaRaio);
            } else if (angle < 0) {
                return (2 * Math.PI * Math.abs(angle) / 360.00) * (radius + diferencaRaio);
            }
        }

        throw new IllegalStateException("Invalid track piece");
    }
    
    public boolean ehCurva() {
    	return radius != 0;
    }
    
    public void setBestSpeed(double speed){
    	if(this.bestSpeed < speed){
    		this.bestSpeed = speed;
    	}
    }
    
    public void setBestAngle(double angle){
    	
    	double angleAbs = Math.abs(angle);
    	if(this.bestAngle < angleAbs){
    		this.bestAngle = angleAbs;
    	}
    }

	@Override
	public String toString() {
		return "TrackPiece [indice=" + indice + ", length=" + length
				+ ", radius=" + radius + ", angle=" + angle + ", maxSpeed="
				+ Arrays.toString(maxSpeed) + ", bestSpeed=" + bestSpeed
				+ ", bestAngle=" + bestAngle + ", hadCrash=" + hadCrash + "]";
	}
    
}
