package dmd.handler;

import java.lang.reflect.Type;

import com.google.common.reflect.TypeToken;

import dmd.Bot;
import dmd.domain.Message;
import dmd.domain.TurboAvaiable;

public class TurboAvaiableHandler implements MessageHandler {

	private static final Type TURBO_AVAILABLE_TYPE = new TypeToken<Message<TurboAvaiable>>() {}.getType();
	
	@Override
	public void handle(String json, Bot bot) {
		
		Message<TurboAvaiable> m = bot.gson.fromJson(json, TURBO_AVAILABLE_TYPE);
		TurboAvaiable turbo = m.data;
		
		bot.hasTurbo = true;
		bot.turbo = turbo;		
	}

}
