package dmd.handler;

import java.lang.reflect.Type;
import java.util.List;

import com.google.common.reflect.TypeToken;

import dmd.Bot;
import dmd.domain.DataRace;
import dmd.domain.Lane;
import dmd.domain.LapFinished;
import dmd.domain.Message;
import dmd.domain.Track;
import dmd.domain.TrackPiece;

public class LapFinishedHandler implements MessageHandler {

	public static final double MAX_SLIP_ANGLE = 48.0;
	
	private static final Type LAP_FINISHED_TYPE = new TypeToken<Message<LapFinished>>() {}.getType();
	
	@Override
	public void handle(String json, Bot bot) {
		
		Message<LapFinished> m = bot.gson.fromJson(json, LAP_FINISHED_TYPE);
		LapFinished lap = m.data;
		
		if(lap.car.equals(bot.id)){
						
			bot.lap++;
			Track track = bot.race.track;
			TrackPiece inicio = track.pieces.get(0);
			TrackPiece current = inicio.nextTrackPiece;
			
			//ajuste maxSpeed das curvas em função do bestAngle
			while(current.indice != inicio.indice){
				
				//saiu de uma curva
				if(!current.ehContinuacaoCurva() && current.previousTrackPiece.ehCurva()) {
					
					List<TrackPiece> curva = current.previousTrackPiece.obterCurvaCompleta();
					boolean hadCrash = TrackPiece.calculaHadCrashTotalCurva(curva);
					
					double bestSlipAngle = TrackPiece.calculaBestAngleTotalCurva(curva);
					double bestSpeed = TrackPiece.calculaBestAngleTotalCurva(curva);
//					double delta = ((bestSlipAngle / MAX_SLIP_ANGLE) - 1) * -1;
//					
//					if(delta > 0){
//						delta = delta*0.1;
//					} else {
//						delta = delta*0.3;
//					}
//					
//					if(hadCrash){
//						delta = delta*3;
//					}
//					if(hadCrash && delta>0){
//						System.out.println("Crash com delta negativo :-O");
//					}
					
					double pontoInversao = 0.3;
					double agressividadeS = 0.5;
					
					double ajuste = (-1.0 * Math.atan((bot.angle-MAX_SLIP_ANGLE*pontoInversao)/agressividadeS) + 1.5)/3.0;
					if(ajuste > 0.9){
						ajuste *= 0.10;
					} else {
						ajuste *= 0.05;
					}
					
					if(hadCrash){
						ajuste = -3*ajuste;
					}
					
					//passou do angulo maximo mas nao bateu, nao ajusta nada
					if(bestSlipAngle > MAX_SLIP_ANGLE && !hadCrash){
						ajuste = 0;
					}
					
					//ajusta maxSpeed em funcao do bestSlipAngle
					for(TrackPiece curvaPiece : curva){
						for(Lane lane : track.lanes){
							curvaPiece.maxSpeed[lane.index] = curvaPiece.maxSpeed[lane.index] + ajuste*curvaPiece.maxSpeed[lane.index];
						}
					}
				}
				
				current = current.nextTrackPiece;
			}
			
			
			//atualiza as retas em função das curvas
			TrackPiece primeiraCurva = inicio.nextTrackPiece;
			while(!primeiraCurva.ehCurva()){
				primeiraCurva = primeiraCurva.nextTrackPiece;
			}
			
			current = primeiraCurva.previousTrackPiece;
			while(current.indice != primeiraCurva.indice){
				
				if(!current.ehCurva()){

					current.maxSpeed = new double[track.lanes.size()];
					for(Lane lane : track.lanes){
						current.maxSpeed[lane.index] = current.nextTrackPiece.maxSpeed[lane.index]*1.20;
					}
				}
				
				current = current.previousTrackPiece;
			}
			
			//apaga registro de crash, bebstAngle e bestSpeed
			for(TrackPiece piece : track.pieces){
				piece.hadCrash = false;
				piece.bestAngle = -1;
				piece.bestSpeed = -1;
			}
		}
	}
}
