package dmd.graph;

import dmd.domain.Lane;
import dmd.domain.Track;
import dmd.domain.TrackPiece;

import java.util.ArrayList;

public class Grafo {

    public String grafo[][];
    public Double pesos[][];

    public int numNodes;
    public int virtualStartNodeIndex = -1;
    public int virtualFinishNodeIndex = -1;

    public Grafo(Double pesos[][], int numNodes) {
        this.pesos = pesos;
        this.numNodes = numNodes;
    }

    public Grafo(Track track) {

        ArrayList<TrackPiece> pieces = track.pieces;
        ArrayList<Lane> lanes = track.lanes;

        //inicializa grafo + 2 nos virtuais
        numNodes = pieces.size()*lanes.size() + 2;
        grafo = new String[numNodes][numNodes];
        pesos = new Double[numNodes][numNodes];

        for(int i=0; i<numNodes; i++) {
            for(int j=0; j<numNodes; j++) {
                grafo[i][j] = "_";
            }
        }

//        System.out.println("numNodes: " + numNodes);

        virtualStartNodeIndex = numNodes - 2;
        virtualFinishNodeIndex = numNodes - 1;
        for (int pieceIndex = 0; pieceIndex < pieces.size(); pieceIndex++) {

            TrackPiece piece = pieces.get(pieceIndex);

            for (int laneIndex = 0; laneIndex < lanes.size(); laneIndex++) {

                Lane lane = lanes.get(laneIndex);

                int nodeIndex = pieceIndex * lanes.size() + lane.index;

                if (pieceIndex == 0) {
                    pesos[virtualStartNodeIndex][nodeIndex] = 0.0;
                    grafo[virtualStartNodeIndex][nodeIndex] = "I_" + nodeIndex+":0.0";
                }

                int nextPieceIndex = pieceIndex + 1;
                int nextNodeIndex = nextPieceIndex * lanes.size() + lane.index;

                //caso seja o ultimo piece aponta par ao no virtual de destino
                if(nextPieceIndex == pieces.size()) {
                    nextNodeIndex = virtualFinishNodeIndex;
                }

                double peso = piece.peso(lane.distanceFromCenter);
                pesos[nodeIndex][nextNodeIndex] = peso;
                grafo[nodeIndex][nextNodeIndex] = nodeIndex + "_" + nextNodeIndex+":"+peso;

                if(piece.canSwitch) {

                    int leftLaneIndex = lane.index - 1;
                    if(leftLaneIndex >= 0){
                        int switchLeftNodeIndex = nextPieceIndex * lanes.size() + leftLaneIndex;
                        grafo[nodeIndex][switchLeftNodeIndex] = nodeIndex+"L"+switchLeftNodeIndex+":"+peso;
                        pesos[nodeIndex][switchLeftNodeIndex] = peso;
                    }

                    int rightLaneIndex = lane.index + 1;
                    if(rightLaneIndex < lanes.size()){
                        int switchRightNodeIndex = nextPieceIndex * lanes.size() + rightLaneIndex;
                        grafo[nodeIndex][switchRightNodeIndex] = nodeIndex+"R"+switchRightNodeIndex+":"+peso;
                        pesos[nodeIndex][switchRightNodeIndex] = peso;
                    }
                }
            }
        }

        /**
        for (int i = 0; i < numNodes; i++) {
            for (int j = 0; j < numNodes; j++) {
                System.out.print("[" + grafo[i][j] + "]");
            }
            System.out.println("");
        }

        System.out.println("-------------------------------");

        for (int i = 0; i < numNodes; i++) {
            for (int j = 0; j < numNodes; j++) {
                System.out.print("[" + pesos[i][j] + "]");
            }
            System.out.println("");
        }
        **/

    }

    static final String enter = System.getProperty("line.separator");
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < numNodes; i++) {
            for (int j = 0; j < numNodes; j++) {
                buffer.append("[" + grafo[i][j] + "]");
            }
            buffer.append(enter);
        }
        return buffer.toString();
    }

}
