package dmd.graph;

import dmd.Bot;
import dmd.domain.TrackPiece;

public interface ThrottleController {

	public void setThrottle(TrackPiece currentPiece, Bot bot);
	
}
