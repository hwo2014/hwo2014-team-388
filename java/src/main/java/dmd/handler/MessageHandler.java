package dmd.handler;

import dmd.Bot;

public interface MessageHandler {

	public void handle(String json, Bot bot);
	
}
