package dmd.handler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.common.reflect.TypeToken;

import dmd.Bot;
import dmd.domain.*;
import dmd.graph.SwitchController;
import dmd.graph.ThrottleController;

public class CarPositionsHandler implements MessageHandler {

	public static final Type CAR_POSITION_TYPE = new TypeToken<Message<ArrayList<CarPosition>>>() {}.getType();
	
	public SwitchController switchController;
	
	public ThrottleController throttleControlle;
	
	public CarPositionsHandler(ThrottleController throttleController, SwitchController switchController) {
		this.throttleControlle = throttleController;
		this.switchController = switchController;
	}

	@Override
	public void handle(String json, Bot bot) {

		Message<ArrayList<CarPosition>> m = bot.gson.fromJson(json, CAR_POSITION_TYPE);
		ArrayList<CarPosition> carPositions = m.data;

		CarPosition myPosition = getMyPosition(bot.id, carPositions);
        bot.currentTrackPiece = bot.race.track.pieces.get(myPosition.piecePosition.pieceIndex);

		// define velocidade
		if (bot.lastPosition != null) {
            TrackPiece lastTrackPiece = bot.race.track.pieces.get(bot.lastPosition.piecePosition.pieceIndex);

			double delta = 0;
			if (bot.lastPosition.piecePosition.pieceIndex == myPosition.piecePosition.pieceIndex) {
				delta = myPosition.piecePosition.inPieceDistance - bot.lastPosition.piecePosition.inPieceDistance;
			}
			else if (lastTrackPiece.length > 0) {
                delta = myPosition.piecePosition.inPieceDistance + (lastTrackPiece.length - bot.lastPosition.piecePosition.inPieceDistance);
            } else if (lastTrackPiece.ehCurva()) {
                Lane lane = bot.race.track.lanes.get(bot.lastPosition.piecePosition.lane.endLaneIndex);
                delta = myPosition.piecePosition.inPieceDistance + (lastTrackPiece.peso(lane.distanceFromCenter) - bot.lastPosition.piecePosition.inPieceDistance);
            } else {
            	delta = bot.speed/60;
            }
            bot.acceleration = delta - bot.speed;
            bot.speed = delta*60;
        }
		
		bot.previousAngle = bot.angle;
        bot.angle = myPosition.angle;
		bot.lastPosition = myPosition;

		boolean enviouSwitch = switchController.shouldSwitch(bot, myPosition, bot.currentTrackPiece);
		//caso não tenha enviado o switch calcula o throttle
		if (!enviouSwitch) {
			
			throttleControlle.setThrottle(bot.currentTrackPiece, bot);
		}
		
		bot.currentTrackPiece.setBestSpeed(bot.speed);
		bot.currentTrackPiece.setBestAngle(bot.angle);
	}

	protected CarPosition getMyPosition(CarId id, ArrayList<CarPosition> carPositions) {

		for (CarPosition position : carPositions) {
			if (position.id.equals(id)) {
				return position;
			}
		}

		return null;
	}
}
