package dmd.dmd.graph;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import dmd.domain.DataRace;
import dmd.domain.Message;
import dmd.graph.Dijkstra;
import dmd.graph.Grafo;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Type;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DijkstraTest {

    static final Gson gson = new Gson();

    @Test
    public void menorCaminho() throws Exception {

        //gabarito em:
        //http://www.geeksforgeeks.org/greedy-algorithms-set-6-dijkstras-shortest-path-algorithm/

        int numNodes = 3;
        Double pesos[][] = new Double[][]{
            {null, 4.0, null, null, null, null, null, 8.0, null},
            {4.0, null, 8.0, null, null, null, null, 11.0, null},
            {null, 8.0, null, 7.0, null, 4.0, null, null, 2.0},
            {null, null, 7.0, null, 9.0, 14.0, null, null, null},
            {null, null, null, 9.0, null, 10.0, null, null, null},
            {null, null, 4.0, null, 10.0, null, 2.0, null, null},
            {null, null, null, 14.0, null, 2.0, null, 1.0, 6.0},
            {8.0, 11.0, null, null, null, null, 1.0, null, 7.0},
            {null, null, 2.0, null, null, null, 6.0, 7.0, null} };

        Grafo grafo = new Grafo(pesos, 9);

        Dijkstra d = new Dijkstra(grafo, 0);

        assertEquals(0, d.dist[0], 0.01);
        assertEquals(4, d.dist[1], 0.01);
        assertEquals(12, d.dist[2], 0.01);
        assertEquals(19, d.dist[3], 0.01);
        assertEquals(21, d.dist[4], 0.01);
        assertEquals(11, d.dist[5], 0.01);
        assertEquals(9, d.dist[6], 0.01);
        assertEquals(8, d.dist[7], 0.01);
        assertEquals(14, d.dist[8], 0.01);

        List<Integer> path = d.minPathTo(4);
        assertEquals((int)path.get(0), 0);
        assertEquals((int)path.get(1), 7);
        assertEquals((int)path.get(2), 6);
        assertEquals((int)path.get(3), 5);
        assertEquals((int)path.get(4), 4);

        assertEquals(5, path.size());
    }

    @Test
    public void menorCaminhoJSon() throws Exception {
        String json = recuperaJson("/race.json");

        Type fooType = new TypeToken<Message<DataRace>>() {}.getType();
        Message<DataRace> m = gson.fromJson(json, fooType);

        Grafo grafo = new Grafo(m.data.race.track);

        Dijkstra d0 = new Dijkstra(grafo,grafo.virtualStartNodeIndex);

        List<Integer> minPath = d0.minPathTo(grafo.virtualFinishNodeIndex);
        assertEquals((int)minPath.get(0), 1);
        assertEquals((int)minPath.get(1), 3);
        assertEquals((int)minPath.get(2), 5);
        assertEquals((int)minPath.get(3), 7);
        
        System.out.println("minPath:");
        for(Integer i: minPath) {
            System.out.println(i);
        }

    }

    private String recuperaJson(String nomeArquivo) throws Exception {
        File fjson = new File(this.getClass().getResource(nomeArquivo).toURI());

        String json = Files.toString(fjson, Charsets.UTF_8);


        return json;
    }
}