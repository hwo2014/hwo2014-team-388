package dmd.graph;

import dmd.Bot;
import dmd.domain.TrackPiece;

public class LinearThrottleController implements ThrottleController {

	public static final double MAX_SLIP_ANGLE = 45.0;
	
	public void setThrottle(TrackPiece currentPiece, Bot bot) {

		int laneIndex = bot.lastPosition.piecePosition.lane.endLaneIndex;
		
		boolean ehRetao = 	!currentPiece.ehCurva() && 
							!currentPiece.nextTrackPiece.ehCurva() && 
							!currentPiece.nextTrackPiece.nextTrackPiece.ehCurva() &&
							!currentPiece.nextTrackPiece.nextTrackPiece.nextTrackPiece.ehCurva();		

		if (ehRetao && bot.hasTurbo) {
			bot.turbo(bot.currentTick);
		} 
		
		double throttle;
		
		double deltaAngle = Math.abs(bot.angle) - Math.abs(bot.previousAngle);
		double nextAngle = Math.abs(bot.angle) + deltaAngle;
		if(nextAngle > MAX_SLIP_ANGLE){
			//significa que na proxima iteração o bot vai ultrapassar o MAX_SLIP_ANGLE
			//mantem uma aceleracao baixa
			throttle = 0.1;
		}
		else{
			/*
			double delta = ((bot.speed / bot.currentTrackPiece.maxSpeed[laneIndex]) - 1) * -1;
			delta = delta * 1.5;
			throttle = bot.throttle + delta * bot.throttle;
			*/
			
//			double maxSpeed = bot.currentTrackPiece.maxSpeed[laneIndex] + 20;
//			throttle = 1.0-Math.pow((bot.speed/maxSpeed),1.0/3.0);
			
			//(-atan((x-400*(4/5))/10) + 3/2)/3
			
			double maxSpeed = bot.currentTrackPiece.maxSpeed[laneIndex];
			double pontoInversao = 0.8;
			double agressividadeS = 7.0;
			
			throttle = (-1.0 * Math.atan((bot.speed-maxSpeed*pontoInversao)/agressividadeS) + 1.5)/3.0;
			
			if(bot.speed > maxSpeed && bot.isTurboOn) {				
				throttle /= bot.turbo.turboFactor;
			}
			
			double nextMaxSpeed = bot.currentTrackPiece.nextTrackPiece.maxSpeed[laneIndex];
			if (nextMaxSpeed < bot.speed * 0.8) {
				throttle*=0.7;
			}
			
//			double pontoInversaoAngle = 0.5;
//			double agressividadeSAngle = 7.0;			
//			double bonusAngle = 0.0;
//			
			if (bot.angle < 20) {
//				bonusAngle = (-1.0 * Math.atan((bot.angle-40.0*pontoInversaoAngle)/agressividadeSAngle) + 1.5)/3.0;
				throttle*=1.2;
			}
//			
//			throttle = throttle + 0.8*bonusAngle; 			
		}
		
		if (throttle > 1) {
			throttle = 1;
		} else if (throttle < 0) {
			throttle = 0;
		}

		bot.throttle(throttle, bot.currentTick);

	}

}
