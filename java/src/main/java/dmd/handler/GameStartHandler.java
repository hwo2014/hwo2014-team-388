package dmd.handler;

import dmd.Bot;

public class GameStartHandler implements MessageHandler {

	@Override
	public void handle(String json, Bot bot) {
		bot.throttle(1.0, bot.currentTick);
	}

}
