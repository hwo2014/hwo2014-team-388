package dmd;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dmd.domain.Lane;
import dmd.domain.TrackPiece;

public class MaxSpeedLogger implements Logger {
	
	public Set<String> log = new LinkedHashSet<String>();
	
	public Map<String, Double> bestSpeedMap = new HashMap<String, Double>();
	
	public MaxSpeedLogger() {
		log.add("anguloTotal;bestSpeed;bestslipAngle;RaioExecucao;throttle");
	}
	
	public void log(Bot bot) {
		
		if(bot.currentTrackPiece == null){
			return;
		}
		
		//saiu de uma curva
		if(!bot.currentTrackPiece.ehContinuacaoCurva() && bot.currentTrackPiece.previousTrackPiece.ehCurva()) {
			
			List<TrackPiece> curva = bot.currentTrackPiece.previousTrackPiece.obterCurvaCompleta();
			
			if(!hadCrash(curva)) {
				
				double anguloTotalCurva = calculaAnguloTotalCurva(curva);
				double bestSpeedCurva = calculaBestSpeedCurva(curva);
				double bestSlipAngle = calculaBestAngleCurva(curva);
				
				Lane lane = bot.race.track.lanes.get(bot.lastPosition.piecePosition.lane.endLaneIndex);
				double distanceFromCenter = lane.distanceFromCenter;
				double raioExecucao;
				if(bot.currentTrackPiece.previousTrackPiece.angle > 0){
					raioExecucao = bot.currentTrackPiece.previousTrackPiece.radius - distanceFromCenter;
				} else{
					raioExecucao = bot.currentTrackPiece.previousTrackPiece.radius + distanceFromCenter;
				}
				
				log.add(anguloTotalCurva + ";" + bestSpeedCurva + ";" + bestSlipAngle + ";" + raioExecucao + ";" + bot.throttle);
				
				registraVelocidade(anguloTotalCurva, raioExecucao, bestSpeedCurva);
				
				if(anguloTotalCurva == 0){
					System.out.println("angulo: 0, curva:");
					for(TrackPiece c : curva) {
						System.out.println("piece.next: " + c.nextTrackPiece);
						System.out.println("piece: " + c);
						System.out.println("piece.previous: " + c.previousTrackPiece);
					}
				}
			} else {
				
				//apaga o flag de crash nos pieces assim como a bestSpeed e o bestAngle
				for(TrackPiece piece : curva){
					piece.hadCrash = false;
					piece.bestSpeed = 0;
					piece.bestAngle = 0;
				}
				
			}
		}		
	}
	
	public void registraVelocidade(double anguloTotalCurva, double raioExecucao, double bestSpeedCurva){
		
		String anguloTotalFmt = String.format("%.1f", anguloTotalCurva); 
		String raioFmt =  String.format("%.1f", raioExecucao);
		String key = anguloTotalFmt + "_" + raioFmt;
		
		Double bestSpeedEver = bestSpeedMap.get(key);
		
		if(bestSpeedEver == null || bestSpeedEver < bestSpeedCurva) {
			bestSpeedMap.put(key, bestSpeedCurva);
		}
	}
	
	public double calculaAnguloTotalCurva(List<TrackPiece> curva) {
		
		double angulo = 0.0;
		
		for(TrackPiece piece : curva){
			angulo += Math.abs(piece.angle);			
		}
				
		return angulo;		
	}
	
	public double calculaBestSpeedCurva(List<TrackPiece> curva) {
		
		double bestSpeed = 0.0;
		for(TrackPiece piece : curva){
			
			if(piece.bestSpeed > bestSpeed) {
				bestSpeed = piece.bestSpeed;
			}
		}
		return bestSpeed;
	}
	
	public double calculaBestAngleCurva(List<TrackPiece> curva) {
		
		double bestAngle = 0.0;
		for(TrackPiece piece : curva){
			
			if(piece.bestAngle > bestAngle) {
				bestAngle = piece.bestAngle;
			}
		}
		return bestAngle;
	}
	
	public boolean hadCrash(List<TrackPiece> curva) {
		
		boolean hadCrash = false;
		for(TrackPiece piece : curva) {
			if(piece.hadCrash){
				return true;
			}
		}
		return hadCrash;
	}

}
