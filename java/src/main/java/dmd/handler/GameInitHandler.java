package dmd.handler;

import java.lang.reflect.Type;
import java.util.List;

import com.google.common.reflect.TypeToken;

import dmd.Bot;
import dmd.domain.DataRace;
import dmd.domain.Message;
import dmd.domain.Track;
import dmd.domain.TrackPiece;
import dmd.graph.Dijkstra;
import dmd.graph.Grafo;
import dmd.graph.InitialMaxSpeedGuess;

public class GameInitHandler implements MessageHandler {

		
	private static final Type DATA_RACE_TYPE = new TypeToken<Message<DataRace>>() {}.getType();
	
	@Override
	public void handle(String json, Bot bot) {

		Message<DataRace> m = bot.gson.fromJson(json, DATA_RACE_TYPE);
		
		bot.race = m.data.race;
		
		Grafo grafo = new Grafo(bot.race.track);

		Dijkstra d = new Dijkstra(grafo,grafo.virtualStartNodeIndex);

        bot.minPath = d.minPathTo(grafo.virtualFinishNodeIndex);

        encadeiaTrackPieces(bot.race.track.pieces);
        
        InitialMaxSpeedGuess initialGuess = new InitialMaxSpeedGuess();
        initialGuess.guess(bot.race.track);
        
        
        for(TrackPiece piece : bot.race.track.pieces){
        	//System.out.println(piece);
        }
	}
	
	
	public void encadeiaTrackPieces(List<TrackPiece> pieces){
		
		
		for(int i=0; i<pieces.size(); i++) {
			
			TrackPiece piece = pieces.get(i);

            piece.indice = i;

			int nextTrackPieceIndex = i+1;
			if(nextTrackPieceIndex == pieces.size()){
				nextTrackPieceIndex = 0;
			}
			piece.nextTrackPiece = pieces.get(nextTrackPieceIndex);
			
			int previousTrackPieceIndex = i-1;
			if(previousTrackPieceIndex < 0){
				previousTrackPieceIndex = pieces.size() - 1;
			}
			piece.previousTrackPiece = pieces.get(previousTrackPieceIndex);
		}
	}
}
