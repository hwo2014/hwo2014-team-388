package dmd;

public class FullLogger implements Logger {

    public StringBuffer buffer = new StringBuffer();
    private String enter = System.getProperty("line.separator");

    public FullLogger() {
        buffer.append("piece index;speed;acceleration;slip angle");
        buffer.append(enter);
    }

    @Override
    public void log(Bot bot) {
        if (bot.currentTrackPiece == null) {
            return;
        }

        buffer.append(bot.currentTrackPiece.indice);
        buffer.append(";");
        buffer.append(bot.speed);
        buffer.append(";");
        buffer.append(bot.acceleration);
        buffer.append(";");
        buffer.append(bot.angle);
        buffer.append(";");
        buffer.append(enter);
    }
}
