package noobbot;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import dmd.Bot;
import dmd.MaxSpeedLogger;
import dmd.graph.ConstantThrottleController;
import dmd.graph.FixedLaneSwitchController;

public class MaxSpeedMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {

		String host = "testserver.helloworldopen.com";
        int port = 8091;
        String botName = "DMD Collector";
        String botKey = "c6V2uQ9WsvqTRw";

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + " /" + botKey);

        
        Map<String, Integer> circuitos = new HashMap<String, Integer>();
        circuitos.put("keimola", 2);
        circuitos.put("germany", 2);
        circuitos.put("usa", 3);
        circuitos.put("france", 2);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
        String now = sdf.format(new Date());
        
        for(Map.Entry<String, Integer> e : circuitos.entrySet()){
        	
        	String circuito = e.getKey();
        	Integer lanes = e.getValue();
        	
        	System.out.println("Circuito: " + circuito);
        	MaxSpeedLogger logger = new MaxSpeedLogger();
            for(double t=0.2; t<=1.01; t+=0.05){

            	if(t > 1.0){
            		t = 1.0;
            	}
                System.out.println("throttle: " + t);
                for(int lane=0; lane<lanes; lane++){

                	System.out.println("lane: " + lane);

                	final Socket socket = new Socket(host, port);
                    final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

                    final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

                	Bot bot = new Bot(reader, writer, new ConstantThrottleController(t), new FixedLaneSwitchController(lane));
                    bot.botName = botName;
                    bot.botKey = botKey;
                    bot.logger = logger;
                    bot.go(circuito);
                }
            }
            
            File f = new File("maxSpeed-"+circuito+"-"+now+".txt");
            BufferedWriter w = new BufferedWriter(new FileWriter(f));
            for(Map.Entry<String, Double> best : logger.bestSpeedMap.entrySet()){
            	String ln = best.getKey()+":"+best.getValue();
            	System.out.println(ln);        	
                w.write(ln+"\n");
            }
            w.close();
        }        
	}

}
