package dmd.domain;

import java.util.ArrayList;
import java.util.List;

public class Track {

	public String id;
	public String name;
	
	public ArrayList<TrackPiece> pieces;

    public ArrayList<Lane> lanes;
    
    public List<TrackPiece> getPiecesSubList(int fromIndex, int numPieces) {
    	
    	List<TrackPiece> subList = new ArrayList<TrackPiece>(numPieces);
    	int index = fromIndex;
    	while(numPieces > 0){

    		subList.add(pieces.get(index));
    		numPieces--;
    		
    	}
    	
    	return subList;
    }
    
    public boolean ehRetao(int fromIndex, int numPieces){
    	
    	int index = fromIndex;
    	while(numPieces > 0){
    		
    		if(pieces.get(index).ehCurva()){
    			return false;
    		}
    		
    		index++;
    		if(index == pieces.size()){
    			index = 0;
    		}
    		
    		numPieces--;
    	}
    	
    	return true;
    	
    }
}
