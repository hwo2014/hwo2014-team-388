package dmd.domain;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class TrackPieceTest {

	@Test
	public void retaNaoEhContinuacaoCurva() {
		
		TrackPiece reta = new TrackPiece();
		reta.angle = 0;
		reta.radius = 0;
		
		TrackPiece curva = new TrackPiece();
		curva.angle = 45;
		curva.radius = 100;
		
		reta.previousTrackPiece = curva;
		
		assertFalse(reta.ehContinuacaoCurva());
	}

	@Test
	public void retaNaoEhContinuacaoCurvaDeReta() {
		
		TrackPiece reta = new TrackPiece();
		reta.angle = 0;
		reta.radius = 0;
		reta.length = 100;
		
		TrackPiece reta2 = new TrackPiece();
		reta2.angle = 0;
		reta2.radius = 0;
		reta2.length = 100;
		
		reta.previousTrackPiece = reta2;
		
		assertFalse(reta.ehContinuacaoCurva());
	}
	
	@Test
	public void curvaEhContinuacaoCurvaComMesmoSentidoERaio() {
		
		TrackPiece curva1 = new TrackPiece();
		curva1.angle = 45;
		curva1.radius = 100;
		
		TrackPiece curva2 = new TrackPiece();
		curva2.angle = 45;
		curva2.radius = 100;
		
		curva1.previousTrackPiece = curva2;
		
		assertTrue(curva1.ehContinuacaoCurva());		
	}
	
	@Test
	public void curvaNaoEhContinuacaoDeCurvaComOutroRaio() {
		
		TrackPiece curva1 = new TrackPiece();
		curva1.angle = 45;
		curva1.radius = 100;
		
		TrackPiece curva2 = new TrackPiece();
		curva2.angle = 45;
		curva2.radius = 200;
		
		curva1.previousTrackPiece = curva2;
		
		assertFalse(curva1.ehContinuacaoCurva());		
	}
	
	@Test
	public void curvaNaoEhContinuacaoDeCurvaComAnguloEmOutroSentido() {
		
		TrackPiece curva1 = new TrackPiece();
		curva1.angle = 45;
		curva1.radius = 100;
		
		TrackPiece curva2 = new TrackPiece();
		curva2.angle = -45;
		curva2.radius = 100;
		
		curva1.previousTrackPiece = curva2;
		
		assertFalse(curva1.ehContinuacaoCurva());		
	}
	
	@Test
	public void obterCurvaCompleta() {
	
		TrackPiece retaInicial = new TrackPiece();
		retaInicial.angle = 0;
		retaInicial.radius = 0;
		
		TrackPiece curva1 = new TrackPiece();
		curva1.angle = 45;
		curva1.radius = 100;
		
		TrackPiece curva2 = new TrackPiece();
		curva2.angle = 45;
		curva2.radius = 100;
		
		TrackPiece retaFinal = new TrackPiece();
		retaFinal.angle = 0;
		retaFinal.radius = 0;
		
		retaFinal.previousTrackPiece = curva2;
		curva2.previousTrackPiece = curva1;
		curva1.previousTrackPiece = retaInicial;
		
		List<TrackPiece> curvaPieces1 = retaFinal.obterCurvaCompleta();
		assertEquals(1, curvaPieces1.size());
		assertTrue(curvaPieces1.contains(retaFinal));
		
		List<TrackPiece> curvaPieces2 = curva2.obterCurvaCompleta();
		assertEquals(2, curvaPieces2.size());
		assertTrue(curvaPieces2.contains(curva1));
		assertTrue(curvaPieces2.contains(curva2));
	}
}
