package dmd.graph;

import dmd.Bot;
import dmd.domain.TrackPiece;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

public class AdaptativeThrottleController implements ThrottleController {

    public Map<TrackPiece, Double> targets = new HashMap<TrackPiece, Double>();
    private double targetAngle = 45.00;

    private double currentThrotle = 0.00;

	@Override
	public void setThrottle(TrackPiece currentPiece, Bot bot) {
        Double newThrottle = currentThrotle;
        Double currentAngle = Math.abs(bot.angle);

        Double targetSpeed = targets.get(currentPiece);
        if (currentPiece.ehContinuacaoCurva()) {
            targetSpeed = targets.get(currentPiece.obterCurvaCompleta().get(0));
        }

        if (targetSpeed == null) {
            targets.put(currentPiece, bot.speed);
        } else if (currentAngle < targetAngle && bot.speed > targetSpeed) {
            targetSpeed = bot.speed;
            if (currentPiece.ehContinuacaoCurva()) {
                targets.put(currentPiece.obterCurvaCompleta().get(0), targetSpeed);
            } else {
                targets.put(currentPiece, targetSpeed);
            }
        }

        if (!currentPiece.ehCurva()) {
            if (currentPiece.nextTrackPiece.ehCurva()) {
                Double s = targets.get(currentPiece.nextTrackPiece);
                if (s != null && bot.speed > s) {
                    newThrottle = 0.0;
                } else {
                    newThrottle = 0.0;
                }
            } else {
                newThrottle = 1.0;
            }
//            Double nextMax = targets.get(currentPiece.nextTrackPiece);
//            if (nextMax != null) {
//                newThrottle = (1 - (bot.speed / nextMax));
//            } else {
//                newThrottle = 0.5;
//            }
        } else {
            newThrottle = (targetAngle - currentAngle) / targetAngle;
        }

        if (newThrottle < 0.00) {
            newThrottle = 0.00;
        }
        if (newThrottle > 1.00) {
            newThrottle = 1.00;
        }

        if (currentThrotle != newThrottle) {
            System.out.println("throttle: "
                    + new BigDecimal(newThrottle).setScale(2, RoundingMode.HALF_UP)
                    + " | angle: "
                    + new BigDecimal(bot.angle).setScale(2, RoundingMode.HALF_UP));
        }
        currentThrotle = newThrottle;

        bot.throttle(newThrottle, bot.currentTick);
	}

}
