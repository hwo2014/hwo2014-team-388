package dmd.graph;

import java.util.HashSet;
import java.util.Set;

import dmd.Bot;
import dmd.domain.CarPosition;
import dmd.domain.TrackPiece;

public class DijkstraSwitchController implements SwitchController{
	
	/**
	 * Set para controlar se mensagem de switch foi enviada para aquela
	 * lap/piece
	 */
	private Set<String> switchMessage = new HashSet<String>();
	
	/**
	 * Decide se precisa trocar de lane
	 * @param bot
	 * @param myPosition
	 * @param currentPiece
	 * @return true caso envie a msg de troca de lane, false caso não envie msg de troca de lane
	 */
	public boolean shouldSwitch(Bot bot, CarPosition myPosition, TrackPiece currentPiece){
		
		String key = myPosition.piecePosition.lap + "" + myPosition.piecePosition.pieceIndex;
		
		if (!switchMessage.contains(key)) {

			switchMessage.add(key);

            int currentLane = myPosition.piecePosition.lane.endLaneIndex;

            TrackPiece nextPiece = currentPiece.nextTrackPiece;
            if (!nextPiece.canSwitch) {
                return false;
            }

            TrackPiece nextNextPiece = nextPiece.nextTrackPiece;
			int bestNode = bot.minPath.get(nextNextPiece.indice);
            int bestLane = bestNode - nextNextPiece.indice * bot.race.track.lanes.size();

            if(currentLane < bestLane){
				//vira pra direita
				bot.switchLane(true, bot.currentTick);
				return true;
			} else if (currentLane > bestLane)  {
				//vira pra esquerda
				bot.switchLane(false, bot.currentTick);
				return true;
			}
            
            return false;

		}
		else {
			return false;
		}
	}

}
