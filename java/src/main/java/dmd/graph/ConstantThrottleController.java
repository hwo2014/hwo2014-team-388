package dmd.graph;

import dmd.Bot;
import dmd.domain.TrackPiece;

public class ConstantThrottleController implements ThrottleController {

	public double throttle;
	
	public ConstantThrottleController(double throttle) {
		this.throttle = throttle;
	}
	
	@Override
	public void setThrottle(TrackPiece currentPiece, Bot bot) {

		bot.throttle(throttle, bot.currentTick);		
	}
}