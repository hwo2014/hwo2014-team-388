package dmd.handler;

import java.lang.reflect.Type;

import com.google.common.reflect.TypeToken;

import dmd.Bot;
import dmd.domain.CarId;
import dmd.domain.Message;

public class YourCarHandler implements MessageHandler {

	private static final Type YOUR_CAR_TYPE = new TypeToken<Message<CarId>>() {}.getType();
	
	@Override
	public void handle(String json, Bot bot) {
		
		Message<CarId> m = bot.gson.fromJson(json, YOUR_CAR_TYPE);
		bot.id = m.data;
	}

}
