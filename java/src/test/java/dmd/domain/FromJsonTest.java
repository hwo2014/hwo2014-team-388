package dmd.domain;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import org.junit.Test;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import dmd.handler.YourCarHandler;

public class FromJsonTest {

	@Test
	public void gameInit() throws Exception {
		
		File jsonFile = new File( this.getClass().getResource( "/gameInit.json" ).toURI() );
		String jsonStr = Files.toString(jsonFile, Charsets.UTF_8);
		
		Gson gson = new Gson(); 

		Type dataRaceType = new TypeToken<Message<DataRace>>() {}.getType();
		Message<DataRace> m = gson.fromJson(jsonStr, dataRaceType);

		assertEquals("gameInit", m.msgType);
		assertNull(m.gameId);
		assertEquals(0, m.gameTick);		
		assertEquals(m.data.getClass(), DataRace.class);
		
		Race race = m.data.race;
		
		Track track = race.track;
		assertEquals(track.id, "indianapolis");
		assertEquals(track.name, "Indianapolis");
		assertEquals(track.pieces.size(), 3);
		
		TrackPiece piece0 = track.pieces.get(0);
		assertEquals(piece0.length, 100.0, 0.01);
		assertFalse(piece0.canSwitch);
		assertEquals(0.0, piece0.radius, 0.01);
		assertEquals(0.0, piece0.angle, 0.01);
		
		TrackPiece piece1 = track.pieces.get(1);
		assertEquals(100.0, piece1.length, 0.01);
		assertTrue(piece1.canSwitch);
		assertEquals(0.0, piece1.radius, 0.01);
		assertEquals(0.0, piece1.angle, 0.01);
		
		TrackPiece piece2 = track.pieces.get(2);
		assertEquals(0, piece2.length, 0.01);
		assertFalse(piece2.canSwitch);
		assertEquals(piece2.radius, 200, 0.01);
		assertEquals(piece2.angle, 22.5, 0.01);
		
		assertEquals(3, track.lanes.size());
		
		Lane lane0 = track.lanes.get(0);
		assertEquals(-20.0, lane0.distanceFromCenter, 0.01);
		assertEquals(0, lane0.index);
		
		Lane lane1 = track.lanes.get(1);
		assertEquals(0, lane1.distanceFromCenter, 0.01);
		assertEquals(1, lane1.index);
		
		Lane lane2 = track.lanes.get(2);
		assertEquals(20.0, lane2.distanceFromCenter, 0.01);
		assertEquals(2, lane2.index);
		
		assertEquals(2, race.cars.size());
		
		Car car0 = race.cars.get(0);
		assertEquals("Schumacher", car0.id.name);
		assertEquals("red", car0.id.color);
		assertEquals(40.0, car0.dimensions.length, 0.01);
		assertEquals(20.0, car0.dimensions.width, 0.01);
		assertEquals(10.0, car0.dimensions.guideFlagPosition, 0.01);
		
		Car car1 = race.cars.get(1);
		assertEquals("Rosberg", car1.id.name);
		assertEquals("blue", car1.id.color);
		assertEquals(30.0, car1.dimensions.length, 0.01);
		assertEquals(10.0, car1.dimensions.width, 0.01);
		assertEquals(15.0, car1.dimensions.guideFlagPosition, 0.01);
		
		//raceSession
		assertEquals(3, race.raceSession.laps);
		assertEquals(30000, race.raceSession.maxLapTimeMs);
		assertTrue(race.raceSession.quickRace);
		
	}
	
	@Test
	public void carPositions() throws Exception{		

		File jsonFile = new File( this.getClass().getResource( "/carPositions.json" ).toURI() );
		String jsonStr = Files.toString(jsonFile, Charsets.UTF_8);
		
		Gson gson = new Gson(); 

		Type carPositionType = new TypeToken<Message<ArrayList<CarPosition>>>() {}.getType();
		Message<ArrayList<CarPosition>> m = gson.fromJson(jsonStr, carPositionType);
				
		assertEquals("carPositions", m.msgType);
		assertEquals("OIUHGERJWEOI",m.gameId);
		assertEquals(1, m.gameTick);		
		assertEquals(m.data.getClass(), ArrayList.class);
	
		ArrayList<CarPosition> carPositions = m.data;
		
		CarPosition position0 = carPositions.get(0);
		assertEquals("Schumacher", position0.id.name);
		assertEquals("red", position0.id.color);
		assertEquals(0.0, position0.angle, 0.0);
		assertEquals(0, position0.piecePosition.pieceIndex);
		assertEquals(0, position0.piecePosition.inPieceDistance, 0.01);
		assertEquals(0, position0.piecePosition.lane.startLaneIndex);
		assertEquals(0, position0.piecePosition.lane.endLaneIndex);
		assertEquals(0, position0.piecePosition.lap);
		
		CarPosition position1 = carPositions.get(1);
		assertEquals("Rosberg", position1.id.name);
		assertEquals("blue", position1.id.color);
		assertEquals(0.0, position1.angle, 45.0);
		assertEquals(0, position1.piecePosition.pieceIndex);
		assertEquals(20, position1.piecePosition.inPieceDistance, 0.01);
		assertEquals(1, position1.piecePosition.lane.startLaneIndex);
		assertEquals(1, position1.piecePosition.lane.endLaneIndex);
		assertEquals(1, position1.piecePosition.lap);
	}
	
	@Test
	public void yourCar() throws Exception {
		
		File jsonFile = new File( this.getClass().getResource( "/yourCar.json" ).toURI() );
		String jsonStr = Files.toString(jsonFile, Charsets.UTF_8);
		
		Gson gson = new Gson(); 
		Type yourCarType = new TypeToken<Message<CarId>>() {}.getType();
		Message<CarId> m = gson.fromJson(jsonStr, yourCarType);
		
		assertEquals("yourCar", m.msgType);
		assertNull(m.gameId);
		assertEquals(0, m.gameTick);		
		assertEquals(m.data.getClass(), CarId.class);
		
		CarId id = m.data;
		
		assertEquals(id.name, "DMD");
		assertEquals(id.color, "red");
	}
	
	@Test
	public void lapFinished() throws Exception {
		File jsonFile = new File( this.getClass().getResource( "/lapFinished.json" ).toURI() );
		String jsonStr = Files.toString(jsonFile, Charsets.UTF_8);
		
		Gson gson = new Gson(); 
		Type lapFinishedType = new TypeToken<Message<LapFinished>>() {}.getType();
		Message<LapFinished> m = gson.fromJson(jsonStr, lapFinishedType);

		assertEquals("lapFinished", m.msgType);
		assertEquals(m.data.getClass(), LapFinished.class);

		LapFinished lap = m.data;
		
		CarId id = lap.car;
		assertEquals(id.name, "DMD");
		assertEquals(id.color, "red");
		
	}
	
	@Test
	public void turboAvaiable() throws Exception {
		
		File jsonFile = new File( this.getClass().getResource( "/turboAvailable.json" ).toURI() );
		String jsonStr = Files.toString(jsonFile, Charsets.UTF_8);
		
		Gson gson = new Gson(); 
		Type turboAvaiableType = new TypeToken<Message<TurboAvaiable>>() {}.getType();
		Message<TurboAvaiable> m = gson.fromJson(jsonStr, turboAvaiableType);

		assertEquals("turboAvailable", m.msgType);
		assertEquals(m.data.getClass(), TurboAvaiable.class);

		TurboAvaiable turbo = m.data; 
		
		assertEquals(500.0, turbo.turboDurationMilliseconds, 0.01);
		assertEquals(30, turbo.turboDurationTicks);
		assertEquals(3.0, turbo.turboFactor, 0.01);
		
	}
}
