package dmd.domain;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import dmd.graph.Grafo;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Type;

public class GraphTest {

    private static final Gson gson = new Gson();

    @Test
    public void criarGrafo() throws Exception {
        String json = recuperaJson("/race.json");

        Type fooType = new TypeToken<Message<DataRace>>() {}.getType();
        Message<DataRace> m = gson.fromJson(json, fooType);

        Grafo grafo = new Grafo(m.data.race.track);
    }

    private String recuperaJson(String nomeArquivo) throws Exception {
//        File fjson = new File(this.getClass().getResource(nomeArquivo).toURI());
        File fjson = new File("C:/Users/upom/IdeaProjects/hwo2014-team-388/java/src/test/resources/race.json");

        String json = Files.toString(fjson, Charsets.UTF_8);


        return json;
    }

}
