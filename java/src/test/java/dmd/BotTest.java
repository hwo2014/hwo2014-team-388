package dmd;


import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import dmd.Bot;
import dmd.handler.MessageHandler;

public class BotTest {

	private Bot bot;
	StringWriter sw;

	@Before
	public void setUp() {
		sw = new StringWriter();

		bot = new Bot(null, new PrintWriter(sw), null, null);
		bot.botName = "name";
		bot.botKey = "key";
		bot.currentTick = 10;
		
	}

	@Test
	public void go() throws Exception {

		final String jsonTest = "{\"msgType\": \"testMsg\", \"data\": \"test data\", \"gameTick\": 15}";
		StringWriter sw = new StringWriter();

		BufferedReader reader = new BufferedReader(new StringReader(jsonTest));
		final PrintWriter writer = new PrintWriter(sw);

		Bot bot = new Bot(reader, writer, null, null);

		MessageHandler testHandler = new MessageHandler() {
			@Override
			public void handle(String json, Bot bot) {
				assertEquals(jsonTest, json);
				bot.ping(15);
			}
		};
		bot.handlers.put("testMsg", testHandler);

		bot.go("");
		assertTrue(sw.getBuffer().toString().endsWith("{\"msgType\": \"ping\", \"gameTick\": 15}\r\n"));
		assertEquals(bot.currentTick, 15);
		assertTrue(bot.msgOnTick[15]);
	}

	@Test
	public void join() {

		bot.join();

		String expectedJson = "{\"msgType\": \"join\", \"data\": {\"name\": \"name\", \"key\": \"key\"}}\r\n";
		assertEquals(expectedJson, sw.getBuffer().toString());		
	}

	@Test
	public void throttle() {

		bot.throttle(0.75,100);

		String expectedJson = "{\"msgType\": \"throttle\", \"data\": 0.75, \"gameTick\": 100}\r\n";
		assertEquals(expectedJson, sw.getBuffer().toString());
	}

	@Test
	public void switchLaneToRight() {

		bot.switchLane(true,100);
		String expectedJson = "{\"msgType\": \"switchLane\", \"data\": \"Right\", \"gameTick\": 100}\r\n";
		assertEquals(expectedJson, sw.getBuffer().toString());
	}
	
	@Test
	public void switchLaneToLeft() {

		bot.switchLane(false, 101);
		String expectedJson = "{\"msgType\": \"switchLane\", \"data\": \"Left\", \"gameTick\": 101}\r\n";
		assertEquals(expectedJson, sw.getBuffer().toString());
	}
	
	@Test
	public void ping() {
		
		bot.ping(123);
		String expectedJson = "{\"msgType\": \"ping\", \"gameTick\": 123}\r\n";
		assertEquals(expectedJson, sw.getBuffer().toString());
	}
	
	@Test
	public void turbo() {
		
		bot.turbo(321);
		String expectedJson = "{\"msgType\": \"turbo\", \"data\": \"Auuuuuuuuuuuuuuuuuuuuuuuuuuuuuu!!!\", \"gameTick\": 321}\r\n";
		assertEquals(expectedJson, sw.getBuffer().toString());		
	}
	
	@Test
	public void tick() {
		
		assertFalse(bot.msgOnTick[10]);
		bot.ping(10);
		assertTrue(bot.msgOnTick[10]);
	}

}
