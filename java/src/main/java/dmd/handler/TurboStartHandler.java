package dmd.handler;

import dmd.Bot;

public class TurboStartHandler implements MessageHandler{

	@Override
	public void handle(String json, Bot bot) {
		
		bot.isTurboOn = true;
		
	}

}
