package noobbot;

import java.io.*;
import java.net.Socket;
import java.util.Map;

import dmd.Bot;
import dmd.FullLogger;
import dmd.MaxSpeedLogger;
import dmd.domain.TrackPiece;
import dmd.graph.*;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + " /" + botKey);

        FullLogger logger = new FullLogger();
        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        Bot bot = new Bot(reader, writer, new LinearThrottleController(), new DijkstraSwitchController());

//        AdaptativeThrottleController controller = new AdaptativeThrottleController();
//        Bot bot = new Bot(reader, writer, controller, new DijkstraSwitchController());
        bot.botName = botName;
        bot.botKey = botKey;
        bot.logger = logger;

        if (args.length == 5) {
            bot.go(args[4]);
        } else {
            bot.go(null);
        }

//        File f = new File("log.csv");
//        BufferedWriter w = new BufferedWriter(new FileWriter(f));
//        w.write(logger.buffer.toString().replace('.', ','));
//        w.close();

//        System.out.println("---------------------------");
//        for (TrackPiece piece : controller.targets.keySet()) {
//            System.out.println(piece.indice + " - " + controller.targets.get(piece));
//        }
    }
}