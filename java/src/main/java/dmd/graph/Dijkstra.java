package dmd.graph;

import java.util.*;

public class Dijkstra {

    /**
     *  The output array.  dist[i] will hold the shortest distance from src to i
     */
    public double dist[];

    /**
     *  sptSet[i] will true if vertex i is included in shortest path tree or shortest distance from src to i is finalized
     */
    public boolean sptSet[];

    public int parent[];

    public int srcNodeIndex;

    Grafo grafo;

    public Dijkstra(Grafo grafo, int srcNodeIndex) {

        this.grafo = grafo;
        this.srcNodeIndex = srcNodeIndex;

        dist = new double[grafo.numNodes];
        sptSet = new boolean[grafo.numNodes];
        parent = new int[grafo.numNodes];

        // Initialize all distances as INFINITE and stpSet[] as false
        for (int i = 0; i < grafo.numNodes; i++){
            dist[i] = Double.MAX_VALUE;
            sptSet[i] = false;
        }

        // Distance of source vertex from itself is always 0
        dist[srcNodeIndex] = 0;

        // Find shortest path for all vertices
        for (int count = 0; count < grafo.numNodes-1; count++)
        {
            // Pick the minimum distance vertex from the set of vertices not
            // yet processed. u is always equal to src in first iteration.
            int u = minDistance(dist, sptSet);

            // Mark the picked vertex as processed
            sptSet[u] = true;

            // Update dist value of the adjacent vertices of the picked vertex.
            for (int v = 0; v < grafo.numNodes; v++)   {

                // Update dist[v] only if is not in sptSet, there is an edge from
                // u to v, and total weight of path from src to  v through u is
                // smaller than current value of dist[v]
                if (!sptSet[v] && grafo.pesos[u][v] != null && dist[u] != Double.MAX_VALUE && (dist[u]+grafo.pesos[u][v]) < dist[v]){
                    dist[v] = dist[u] + grafo.pesos[u][v];
                    parent[v] = u;
                }
            }
        }
    }

    int minDistance(double dist[], boolean sptSet[]){
        // Initialize min value
        double min = Double.MAX_VALUE;
        int min_index = 0;

        for (int v = 0; v < grafo.numNodes; v++)
            if (sptSet[v] == false && dist[v] <= min) {
                min = dist[v];
                min_index = v;
            }

        return min_index;
    }

    public List<Integer> minPathTo(int destNodeIndex) {
        List<Integer> path = new ArrayList<Integer>();
        minPathTo(destNodeIndex, path);
        return path;
    }

    private void minPathTo(int destNodeIndex, List<Integer> path) {

    	if(destNodeIndex != grafo.virtualStartNodeIndex && destNodeIndex != grafo.virtualFinishNodeIndex){
    		path.add(destNodeIndex);
    	}

        if(destNodeIndex == srcNodeIndex){
            Collections.reverse(path);
        } else {
            minPathTo(parent[destNodeIndex], path);
        }
    }
}